import env


class Environment:
    global getDataOut
    global getData
    global getFutureDistance
    global dateTimeColumnIndex
    global datalocation
    global incrementBy
    global divi
    global divisions
    global indexCol
    global columns
    global colOfInterest
    global minPositivePercentageForTrade
    global minNegativePercentageForTrade
    global startingFunds

    def __init__(self):
        self.getDataOut = env.getDataOut
        self.getData = env.getData
        self.getFutureDistance = env.getFutureDistance
        self.dateTimeColumnIndex = env.dateTimeColumnIndex
        self.datalocation = env.datalocation
        self.incrementBy = env.incrementBy
        # divide env.divi data by this amount, for sliding window this is the size of the window#
        self.divisions = env.divisions
        self.indexCol = env.indexCol
        self.columns = env.columns
        self.colOfInterest = env.colOfInterest
        self.minPositivePercentageForTrade = env.minPositivePercentageForTrade
        self.minNegativePercentageForTrade = env.minNegativePercentageForTrade
        self.startingFunds = env.startingFunds
