from distutils.core import setup

setup(name='mastersPredictionAlgo',
      version='1.0',
      description='Python Distribution Utilities',
      author='James Mackle',
      author_email='jmackle04@qub.ac.uk',
      url='https://gitlab.com/jmackle2906/tradingalgo',
      packages=[],
     )