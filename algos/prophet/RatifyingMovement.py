from numpy import cov
from numpy import std
import pandas as pd


def record_prediction_correctness_basic_direction(out_df_with_changes: pd.DataFrame):
    global correct_total, index
    correct_total = []
    for index, itemRow in out_df_with_changes.iterrows():

        if itemRow["ychange"] > 0 and itemRow["yhatchange"] > 0:
            correct_total.append("True")
        elif itemRow["ychange"] < 0 and itemRow["yhatchange"] < 0:
            correct_total.append("True")
        else:
            correct_total.append("False")
    return correct_total


def buy_sell_or_hold_by_magnitude(out_df_with_changes, min_pos_prediction, min_negative_prediction):
    # todo change to % thresholds so they work for different currencies

    global action, index
    action = []
    for index, item_row in out_df_with_changes.iterrows():

        if 100 * (item_row["yhatchange"] / item_row["y"]) > min_pos_prediction:
            action.append("buy")
        elif 100 * (item_row["yhatchange"] / item_row["y"]) < -(min_negative_prediction):
            action.append("short")
        else:
            action.append("nothing")
    return action


# covariance

def spearman_correlation(list1, list2):
    return cov(list1, list2) / (std(list1) * std(list2))


def buying_success_rate(dataframe):
    gains_loses = []

    for index, item_row in dataframe.iterrows():
        if item_row["action"] == "buy" and item_row["correctPreds"] == True:
            gains_loses.append("gain")

        elif item_row["action"] == "short" and item_row["correctPreds"] == True:
            gains_loses.append("gain")

        elif item_row["action"] == "nothing":
            gains_loses.append("nothing")

        else:
            gains_loses.append("loss")

    return gains_loses


def get_buying_equity_change(dataframe, capital):
    capital_time_series = []

    for index, item_row in dataframe.iterrows():
        if item_row["action"] == "buy":
            previous_value = item_row["y"] - item_row["ychange"]
            capital = capital * (item_row["y"] / previous_value)
        if item_row["action"] == "short":
            previous_value = item_row["y"] - item_row["ychange"]
            capital = abs(capital * (1 + (1 - item_row["y"] / previous_value)))

        capital_time_series.append(capital)

    return capital_time_series


def change_in_equity(dataframe, capital):
    for index, item_row in dataframe.iterrows():
        if item_row["action"] == "buy":
            previousValue = item_row["y"] - item_row["ychange"]
            capital = capital * (item_row["y"] / previousValue)
        if item_row["action"] == "short":
            previousValue = item_row["y"] - item_row["ychange"]
            capital = abs(capital * (1 + (1 - item_row["y"] / previousValue)))

    return capital
