from __future__ import annotations
import pandas as pd

import Environment
import random
from datetime import datetime, timedelta

from algos.prophet.PredictionMethodStrategy import PredictionMethodStrategy


class DataSelectionStrategyRandomGuessing(PredictionMethodStrategy):
    def get_predictions(self, data: pd.DataFrame, environment: Environment) -> pd.DataFrame:
        new_df = []
        final_date = '2020-05-15'
        value = 1
        data = pd.DataFrame(data.iloc[::-1])
        for index, itemRow in data.iterrows():

            if value == 1:
                prediction = float(itemRow['y'])

            value = int(itemRow['y'])

            final_date = itemRow['ds']
            change = self.generate_change(value)
            prediction = value + (change * prediction)
            itemRow['yhat'] = prediction
            new_df.append(itemRow)

        for i in range(1, environment.getFutureDistance):
            res = (datetime.strptime(final_date, '%Y-%m-%d') + timedelta(days=i)).strftime('%Y-%m-%d')
            value = self.generate_change(value)

            prediction = value + (change * prediction)
            new_row = pd.Series([res, 0, prediction], index=['ds', 'y', 'yhat'])

            new_df.append(new_row)

        out_df = pd.DataFrame(new_df)
        out_df.columns = ["ds", "y", "yhat"]

        out_df = pd.DataFrame(out_df.iloc[::-1])

        return out_df

    def generate_change(self, seed):
        random.seed(seed)

        initial = random.randrange(0, 5000)
        if initial > 4000:
            initial = random.randrange(500, 5000)
        if initial > 4999:
            initial = random.randrange(5000, 10 ** 5)

        divide_to_under_zero = initial / 10 ** 5
        random_positive_or_negative = divide_to_under_zero * random.uniform(-1, 1)
        print(random_positive_or_negative)
        return random_positive_or_negative
