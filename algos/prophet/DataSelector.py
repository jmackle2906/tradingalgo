import Environment
from algos.prophet.DataSelectionStrategy import DataSelectionStrategy
import pandas as pd
from typing import List


class DataSelector:
    def __init__(self, strategy: DataSelectionStrategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> DataSelectionStrategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: DataSelectionStrategy) -> None:
        self._strategy = strategy

    def select_data(self, data: pd.DataFrame, environment: Environment) -> List[pd.DataFrame]:
        result = self.strategy.perform_data_selection(data, environment)
        return result
