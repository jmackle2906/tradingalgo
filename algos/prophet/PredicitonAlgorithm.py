import Environment
from algos.prophet.PredictionMethodStrategy import PredictionMethodStrategy
import pandas as pd


class PredictionAlgorithm:
    def __init__(self, strategy: PredictionMethodStrategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> PredictionMethodStrategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: PredictionMethodStrategy) -> None:
        self._strategy = strategy

    def execute(self, data: pd.DataFrame, environment: Environment) -> pd.DataFrame:
        result = self.strategy.get_predictions(data, environment)
        return result
