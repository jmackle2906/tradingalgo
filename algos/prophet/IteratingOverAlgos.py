from multiprocessing import Pool

import Environment
import numpy as np
import seaborn as sb
from matplotlib import pyplot as plt

from algos.prophet.DataSelectionStrategyDivideData import DataSelectionStrategyDivideData
from algos.prophet.DataSelectionStrategySlidingWindow import DataSelectionStrategySlidingWindow
from algos.prophet.DataSelector import DataSelector
from algos.prophet.EquityPredictionAlgorithm import run_through_all_dataframes
from algos.prophet.PredicitonAlgorithm import PredictionAlgorithm
from algos.prophet.PredictionMethodStrategyProphet import PredictionMethodStrategyProphet
from algos.prophet.PredictionMethodStrategyRandomGuessing import DataSelectionStrategyRandomGuessing


def test_method(input):
    return (input * 0.8)


def run_algorithm(divisions):
    random_algorithm = PredictionAlgorithm(DataSelectionStrategyRandomGuessing())

    prophet = PredictionAlgorithm(PredictionMethodStrategyProphet())
    env_object = Environment.Environment()
    env_object.divisions = divisions
    sliding_window = DataSelector(DataSelectionStrategySlidingWindow())
    dividing_data = DataSelector(DataSelectionStrategyDivideData())
    DataSelector(DataSelectionStrategyDivideData())

    division_random = run_through_all_dataframes(env_object, random_algorithm, dividing_data)

    return [division_random, env_object]


def main():
    divisions = [2, 10]
    pool = Pool(3)
    results = pool.map(run_algorithm, divisions)

    return results


def make_histogram(list, label):
    label = str(label.divisions)
    plt.xlim([min(list) - 10, max(list) + 10])
    plt.title("normal distribution " + label)
    plt.xlabel("percentage accuracy")
    plt.ylabel("count")
    plt.show()


def seaborn_density_plot(list, label):
    label = str(label.divisions)
    sb.displot(list, kde=True, bins=np.arange(0, 100, 5), color='darkblue')
    plt.xticks(np.arange(0, 100, 5))
    plt.title("normal distribution " + label)
    plt.xlabel("percentage accuracy")
    plt.ylabel("count")
    plt.show()


if __name__ == '__main__':
    results = main()
    if type(results) == list:
        for result in results:
            label = result[1]
            seaborn_density_plot(result[0][1], label)
    elif type(results) == tuple:
        label = results[1]
        seaborn_density_plot(results[2], label)
