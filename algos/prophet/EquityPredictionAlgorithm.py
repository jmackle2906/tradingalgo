import pandas as pd

import Environment
import algos.prophet.DataPreProcessor as dataPreProcessor
import algos.prophet.DataFrameArrayEdit as dfEd
import algos.prophet.RatifyingMovement as ratmov
from algos.prophet.DataSelector import DataSelector
from algos.prophet.PredicitonAlgorithm import PredictionAlgorithm


def get_analyses_and_output_predictions(prediction_algorithm: PredictionAlgorithm, dataframe, percentage_accuracies,
                                        percentage_prediction_accuracies, environment: Environment):
    x_train = dataframe[environment.getFutureDistance:(len(dataframe.index))]
    prediction = prediction_algorithm.execute(x_train, environment)
    out_df = dfEd.add_predictions_to_og_df(dataframe, prediction)
    out_df_with_changes = dfEd.add_changes_to_dataframe(out_df)
    correct_total = ratmov.record_prediction_correctness_basic_direction(out_df_with_changes)
    out_df_with_changes["correctPreds"] = correct_total
    predictions = pd.DataFrame(out_df_with_changes.iloc[-14:])
    predictions["action"] = ratmov.buy_sell_or_hold_by_magnitude(predictions,
                                                                 environment.minPositivePercentageForTrade,
                                                                 environment.minNegativePercentageForTrade)
    equity = ratmov.get_buying_equity_change(predictions, environment.startingFunds)
    predictions["equity"] = equity
    percentage_accuracies, percentage_prediction_accuracies = dfEd.get_percentage_accuracies(out_df_with_changes,
                                                                                             percentage_accuracies,
                                                                                             percentage_prediction_accuracies)
    return percentage_accuracies, percentage_prediction_accuracies, predictions


# so many lists being declare, this smells
# TODO reduce number of lists being kept in memory at any one time if they
#  arent being used delete them, or write them to a file if necessary

# buyingSuccessRecord = []
# tests = []

# newcount = 0
# spearmanRes = []
# covRes = []
# dfDescribe = []
# correlations = []


def run_through_all_dataframes(environment: Environment, algorithm: PredictionAlgorithm,
                               data_selector: DataSelector):
    dataframes = dataPreProcessor.prepare_data_selection(environment, data_selector)

    initial_funds = environment.startingFunds

    percentage_accuracies = []
    percentage_pred_accuracies = []
    out_dfs_with_changes = []
    for dataframe in dataframes:


        percentage_accuracies, percentage_pred_accuracies, prediction = get_analyses_and_output_predictions(algorithm,
                                                                                                            dataframe,
                                                                                                            percentage_accuracies,
                                                                                                            percentage_pred_accuracies,
                                                                                                            environment)
        initial_funds = ratmov.change_in_equity(prediction, initial_funds)
        out_dfs_with_changes.append(prediction)
    return initial_funds, percentage_pred_accuracies

# separate out into two separate paths, one where the predictions occur the other where the predictions are analysed, pass a list of the prediction dataframes


# initialFunds, percentagePredAccuracies = runThroughAllDataframes()


#     spearman = ratmov.spearmanCorrelation(outDFwithChanges.iloc[:,3],outDFwithChanges.iloc[:,4])
#     correlation = ratmov.corrcoef(outDFwithChanges.iloc[:,3],outDFwithChanges.iloc[:,4])
#     descriptionOfData = outDFwithChanges.describe()
#     covariance = ratmov.cov(outDFwithChanges.iloc[:,3],outDFwithChanges.iloc[:,4])
# # check total correct predictions then just check 14 days unseen
#     correlations.append(correlation)
#     dfDescribe.append(descriptionOfData)
#     spearmanRes.append(spearman)
#     covRes.append(covariance)


# todo get the spearman and other stat agregates but make them into one or two cohesive stats to compare to other ones
