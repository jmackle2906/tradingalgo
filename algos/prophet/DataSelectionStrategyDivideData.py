import pandas as pd
import Environment
from typing import List

from algos.prophet.DataSelectionStrategy import DataSelectionStrategy


class DataSelectionStrategyDivideData(DataSelectionStrategy):
    def perform_data_selection(self, dataframe: pd.DataFrame, environment: Environment) -> List[pd.DataFrame]:

        divisions = environment.divisions
        row_count = len(dataframe.index)
        number_of_divisions = self.get_number_of_divisions(row_count, divisions)
        counter = 1
        next = number_of_divisions
        divided_dataframes = []
        while counter <= divisions:
            if counter == divisions:
                divided_dataframes.append(dataframe[(next - number_of_divisions):next])
                counter += 1

            else:
                counter += 1
                divided_dataframes.append(dataframe[(next - number_of_divisions):next])
                next = number_of_divisions * counter

        return divided_dataframes

    def get_number_of_divisions(self, row_count, divisions):
        return int(row_count / divisions)
