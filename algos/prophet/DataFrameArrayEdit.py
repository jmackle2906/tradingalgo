import pandas as pd
import statistics


def add_changes_to_dataframe(dataframe):
    y_change = []
    yhat_change = []
    previous_row = []
    initialised = False
    for index, itemRow in dataframe.iterrows():
        if initialised == False:
            previous_row = itemRow
            initialised = True

        else:
            ychange_value = previous_row["y"] - itemRow["y"]
            yhat_change_value = previous_row["yhat"] - itemRow["yhat"]
            y_change.append(ychange_value)
            yhat_change.append(yhat_change_value)
            previous_row = itemRow
    dataframe = dataframe[1:]
    dataframe["ychange"] = y_change
    dataframe["yhatchange"] = yhat_change
    return dataframe


def get_percentage_accuracies(out_df_with_changes, percentage_accuracies, percentage_prediction_accuracies):
    correct_count = out_df_with_changes["correctPreds"].str.count("True").sum()
    false_count = out_df_with_changes["correctPreds"].str.count("False").sum()

    if correct_count > 0:
        predictions = out_df_with_changes.iloc[-14:]
        correct_count_prediction = predictions["correctPreds"].str.count("True").sum()
        false_count_prediction = predictions["correctPreds"].str.count("False").sum()
        percentage_accuracies.append((100 * (correct_count / (false_count + correct_count))))

        if correct_count_prediction > 0:
            percentage_prediction_accuracies.append(
                (100 * (correct_count_prediction / (false_count_prediction + correct_count_prediction))))
        else:
            percentage_prediction_accuracies.append(0)
    else:
        percentage_accuracies.append(0)

    return percentage_accuracies, percentage_prediction_accuracies


def add_predictions_to_og_df(item, prediction):
    new_df = []
    for index, item_row in item.iterrows():
        prediction_row = prediction.loc[prediction["ds"] == item_row["ds"]]
        item_row = item_row.append(prediction_row["yhat"], ignore_index=True)
        new_df.append(item_row)
    out_df = pd.DataFrame(new_df)
    out_df.columns = ["date", "y", "yhat"]
    out_df = out_df.iloc[::-1]
    return out_df


def standard_deviation_based_accuracy_score(percentage_prediction_accuracies):
    low_range = statistics.median(percentage_prediction_accuracies) - statistics.stdev
    return low_range


def get_average_accuracy(percentage_pred_accuracies):
    total = 0
    counter = 0
    for number in percentage_pred_accuracies:
        total = total + number
        counter += 1
    average_accuracy = total / counter
    return average_accuracy


def get_divisions():
    import DataPreProcessor
    out = DataPreProcessor.d
    return out
