import pandas as pd


# todo set up an actual env file, make variables global then call them
import Environment
from algos.prophet.DataSelector import DataSelector


def prepare_data_selection(environment: Environment, data_selector: DataSelector):
    data = environment.getData
    read_data = pd.read_csv(data)

    df = pd.DataFrame(read_data)
    df.drop(df.tail(4).index, inplace=True)

    column_number = df.columns.get_loc(environment.colOfInterest)
    dataframe = df.iloc[:, [environment.dateTimeColumnIndex, column_number]]

    diff_col = environment.colOfInterest + " Diff"
    df = df.set_index(environment.indexCol)
    df[diff_col] = df[environment.colOfInterest].diff()
    df.drop(df.head(1).index, inplace=True)

    dataframe = dataframe.rename(columns={environment.indexCol: "ds", environment.colOfInterest: "y"})

    return data_selector.select_data(dataframe, environment)
