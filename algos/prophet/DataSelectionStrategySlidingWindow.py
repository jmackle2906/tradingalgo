import pandas as pd
from typing import List
import Environment
from algos.prophet.DataSelectionStrategy import DataSelectionStrategy


class DataSelectionStrategySlidingWindow(DataSelectionStrategy):
    def perform_data_selection(self, dataframe: pd.DataFrame, environment: Environment) -> List[pd.DataFrame]:
        increments = environment.incrementBy
        number_of_divisions = environment.divisions
        # todo handle divide by zero, maybe default anything lower than 2 to 2
        row_count = len(dataframe.index)
        top = int(row_count / number_of_divisions)

        sliding_dataframes = []
        bottom = 0
        while top < row_count:
            sliding_dataframes.append(dataframe[bottom:top])
            bottom += increments
            top += increments
        return sliding_dataframes
