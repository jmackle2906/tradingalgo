from __future__ import annotations
from abc import ABC, abstractmethod
import pandas as pd

import Environment


class PredictionMethodStrategy(ABC):
    @abstractmethod
    def get_predictions(self, data: pd.DataFrame, environment: Environment) -> pd.DataFrame:
        pass
