from __future__ import annotations
from abc import ABC, abstractmethod
import pandas as pd
from typing import List
import Environment


class DataSelectionStrategy(ABC):
    @abstractmethod
    def perform_data_selection(self, dataframe: pd.DataFrame, environment: Environment) -> List[pd.DataFrame]:
        pass
