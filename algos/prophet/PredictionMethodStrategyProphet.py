import algos.prophet.PredictionMethodStrategy as predAbs
import pandas as pd
import fbprophet as prophet
import Environment


class PredictionMethodStrategyProphet(predAbs.PredictionMethodStrategy):
    def get_predictions(self, data: pd.DataFrame, environment: Environment) -> pd.DataFrame:
        future_distance = environment.getFutureDistance
        m = prophet.Prophet(daily_seasonality=True, weekly_seasonality=True)
        m.fit(data)
        future = m.make_future_dataframe(periods=future_distance)
        prediction = m.predict(future)

        return prediction
